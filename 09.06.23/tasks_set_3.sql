--Set 3


--Alter table: Add a column 'Project_Status' of varchar type to the 'Project_Details' table.

ALTER TABLE project_details ADD project_status VARCHAR2(50);



--Update operation: Update the 'Project_Details' table to set 'Project_Status' as 'Completed'
--for all projects whose 'End_Date' is before today's date.

UPDATE project_details
SET
    project_status = 'Completed'
WHERE
    end_date < sysdate;
    
commit;


--Subquery: Write a query to find employees who earn more than the average salary.

SELECT
    *
FROM
    employees
WHERE
    salary > (
        SELECT
            AVG(salary)
        FROM
            employees
    );




--Conversion Function: Write a query to convert the hire date in the 'Employees' table to a string
--in the 'DD-MON-YYYY' format.

SELECT
    to_char(hire_date, 'DD-MM-YYYY')
FROM
    employees;
    

--Full Outer Join: Write a query to get all employees and all departments, regardless of whether
--the employee is assigned to a department or not.

SELECT
    *
FROM
    employees   e
    FULL JOIN departments d ON e.department_id = d.department_id;

