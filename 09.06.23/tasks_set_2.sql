--Set 2


--Alter table: Add a column 'Salary' of number type to the 'Employee_Details' table.

ALTER TABLE employee_details ADD salary NUMBER;


--Update operation: Update the 'Employees' table to set the 'commission_pct' to 0.2 for all employees
--with the 'job_id' of 'SA_REP'.

UPDATE employees
SET
    COMMISSION_PCT = 0.2
WHERE
    job_id = 'SA_REP'; 
    
    
--Aggregate Function: Write a query to find the average salary in the 'Employees' table. 
SELECT
    AVG(salary)
FROM
    employees;


--Date Function: Write a query to return the hire date and number of years since hire for each employee
--in the 'Employees' table. Use the SYSDATE and EXTRACT functions.

SELECT
    sysdate,
    EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM hire_date)
FROM
    employees;



--Left Join: Write a query to get all employees and their department names, including those not assigned to
--a department.

SELECT
    employee_id,
    department_name
FROM
    employees e
left join departments d ON e.department_id = d.department_id;
