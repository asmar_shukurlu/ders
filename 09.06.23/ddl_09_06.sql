//create,constraint,keys
//yeni customers cedvelinin yaradilmasi,mehdudiyyetlerin qoyulmasi,
//acar elave edilmesi

CREATE TABLE customers_ (
    customer_id NUMBER PRIMARY KEY,
    first_name  VARCHAR2(50) NOT NULL,
    last_name   VARCHAR2(50) NOT NULL,
    adress     VARCHAR(100)
);

CREATE TABLE stores_ (
    stores_id NUMBER PRIMARY KEY,
    location_ VARCHAR2(100),
    phone     VARCHAR2(50)
);

//drop - movcud olan customer adli cedvelin silinmesi
DROP TABLE stores_;


//alter - phone adli yeni sutunun elave olunmasi

ALTER TABLE customers_
ADD     phone       VARCHAR2(50);

//alter-sehv yazilan sutun adinin deyisdirilmesi

ALTER TABLE customers_
RENAME COLUMN adress to address;



