// CREATE - Cedvellerin yaradilmasi

CREATE TABLE patients (
    patient_id NUMBER PRIMARY KEY,
    first_name  VARCHAR2(50) NOT NULL,
    last_name   VARCHAR2(50) NOT NULL,
    birt_date  DATE NOT NULL,
    phone       VARCHAR2(50) NOT NULL,
    adress_     VARCHAR(100) NOT NULL,
);

CREATE TABLE hospitals (
    hospital_id NUMBER PRIMARY KEY,
    location_ VARCHAR2(100),
);

CREATE TABLE doctors (
    doctor_id NUMBER PRIMARY KEY,
    first_name  VARCHAR2(50) NOT NULL,
    last_name   VARCHAR2(50) NOT NULL,
    phone       VARCHAR2(50) NOT NULL,
);

CREATE TABLE hospital_deps (
    dep_id NUMBER PRIMARY KEY,
    name_ VARCHAR2(100),
);


//alter - patients cedveline gender adli yeni sutunun elave olunmasi

ALTER TABLE patients_ ADD gender VARCHAR2(10) NOT NULL



//drop - movcud olan hospital_deps adli cedvelin silinmesi

DROP TABLE hospital_deps;

