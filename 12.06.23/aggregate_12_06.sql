//COUNT - sayini gosterir

SELECT
    COUNT(*)
FROM
    departments;


//SUM
SELECT
    SUM(salary)
FROM
    employees;


//MIN

SELECT
    MIN(salary)
FROM
    employees;



//MAX

SELECT
    MAX(salary)
FROM
    employees;
    
    
//AVG

SELECT
    AVG(salary)
FROM
    employees;
    
//MEDIAN