//Select,Projection,From,Filter,Group by,Having,Order,Fetch

SELECT
    *
FROM
    departments;

SELECT
    department_id,
    department_name
FROM
    departments;
    
    

SELECT
    MAX(salary)
FROM
    employees;



SELECT
    MIN(salary)
FROM
    employees;

    

SELECT
    COUNT(*)
FROM
    departments;



SELECT
    first_name,
    last_name,
    salary
FROM
    employees
ORDER BY
    salary;
    