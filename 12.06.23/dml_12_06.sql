// insert - stores_ cedveline melumatlarin elave edilmesi

INSERT INTO patients (
    patient_id,
    first_name,
    last_name,
    birt_date,
    phone,
    adress_,
    gender
) VALUES (
    1,
    'Esmer',
    'Shukurlu',
    TO_DATE('03/06/2002', 'DD/MM/YYYY'),
    '0123444444 ',
    ' Azadligh prospekti ',
    'f'
);
    commit;
    
INSERT INTO patients (
    patient_id,
    first_name,
    last_name,
    birt_date,
    phone,
    adress_,
    gender
) VALUES (
    2,
    'Ilahe',
    'Memmedova',
    TO_DATE('22/02/2002', 'DD/MM/YYYY'),
    '0555555555',
    ' Xirdalan seheri ',
    'f'
);

commit;
 
 
//update - movcud melumatin deyisdirilmesi
// id'si 4-e beraber olan xestenin nomresinin deyisilmesi

UPDATE patients SET phone = '0102222222' WHERE patient_id = 2;
commit;


//delete- melumatin silinmesi

DELETE FROM patients WHERE patient_id=2;
commit;