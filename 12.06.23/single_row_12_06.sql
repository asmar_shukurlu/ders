//UPPER- adin butun herflerinin boyuye cevrilmesi

SELECT
    upper(first_name)
FROM
    patients;
    
    
    
//LENGTH - adlarin uzunlugunun tapilmasi, name_length aliasi ile

SELECT
    patient_name,
    length(patient_name) AS name_length
FROM
    patients;



//ROUND - yuvarlaqlasdirir

SELECT
    round(398.3645,-1)
FROM
    dual;



//TRUNC

SELECT
    trunc(11.5682, 3)
FROM
    dual;
    


//SYSDATE - sistemin indiki vaxtini gosterir


